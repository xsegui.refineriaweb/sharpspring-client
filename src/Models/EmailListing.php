<?php

/**
 * This file is part of the Rw/sharpspring-api package.
 *
 * (c) David Llobell <dllobellmoya@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class EmailListing
 *
 * An Email Listing contains information about a group of emails, including the id, HTML content, subject line, and thumbnail image.
 *
 * @package Rw\sharpspring-api
 * @author  David Llobell  <dllobellmoya@gmail.com>
 */
class EmailListing extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'createTimestamp',
        'title',
        'subject',
        'thumbnail'
    ];
}
