<?php

/**
 * This file is part of the Rw/sharpspring-api package.
 *
 * (c) David Llobell <dllobellmoya@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rw\SharpspringApi\Models;

use Rw\SharpspringApi\Model;

/**
 * Class Lists
 *
 * A List is a segmented audience of leads in SharpSpring.
 *
 * @package Rw\sharpspring-api
 * @author  David Llobell  <dllobellmoya@gmail.com>
 */
class Lists extends Model
{
    /**
     * @inheritDoc
     */
    protected $attributes = [
        'id',
        'name',
        'memberCount',
        'removedCount',
        'createTimestamp',
        'description'
    ];
}
