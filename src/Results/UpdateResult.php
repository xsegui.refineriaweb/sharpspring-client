<?php

/**
 * This file is part of the Rw/sharpspring-api package.
 *
 * (c) David Llobell <dllobellmoya@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rw\SharpspringApi\Results;

use Rw\SharpspringApi\Result;

/**
 * Class UpdateResult
 *
 * UpdateResult contains information about the success of an update operation, and an array of any errors associated with the operation.
 *
 * @package Rw\sharpspring-api
 * @author  David Llobell  <dllobellmoya@gmail.com>
 */
class UpdateResult extends Result
{
}
