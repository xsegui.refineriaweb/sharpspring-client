<?php

/**
 * This file is part of the Rw/sharpspring-api package.
 *
 * (c) David Llobell <dllobellmoya@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'account_id' => env('SHARPSPRING_ACCOUNT_ID', ''),
    'secret_key' => env('SHARPSPRING_SECRET_KEY', '')
];
