<?php

/**
 * This file is part of the Rw/sharpspring-api package.
 *
 * (c) David Llobell <dllobellmoya@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rw\SharpspringApi\Contracts;

/**
 * Interface Jsonable
 *
 * @package Rw\sharpspring-api
 * @author  David Llobell  <dllobellmoya@gmail.com>
 */
interface Jsonable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0);
}
