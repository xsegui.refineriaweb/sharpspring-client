<?php

/**
 * This file is part of the dllobell/sharpspring-api package.
 *
 * (c) David Llobell <dllobellmoya@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rw\SharpspringApi\Tests;

use PHPUnit_Framework_TestCase as PHPUnit;

/**
 * Class TestCase
 *
 * @author  David Llobell  <dllobellmoya@gmail.com>
 */
class TestCase extends PHPUnit
{

    public function __construct()
    {
        parent::__construct();
    }

    public function setUp()
    {
        parent::setUp();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

}
